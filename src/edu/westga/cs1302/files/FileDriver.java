package edu.westga.cs1302.files;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/** driver for the file application
 * 
 * @author Nicholas Ponder
 * @version 7/9/2021
 *
 */
public class FileDriver {

	/** driver for the file application
	 * 
	 * @precondition none 
	 * 
	 * @param args none
	 */
	public static void main(String[] args) {
		try {
			File theFile = new File("data.txt");
			FileWriter outFile = new FileWriter(theFile, true);
			outFile.write("\nHello");
			outFile.close();
		} catch (IOException anException) {
			System.out.println("There was a problem with the file");
		}
	}

}
